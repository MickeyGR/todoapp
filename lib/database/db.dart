import 'package:sqflite/sqflite.dart';
import 'dart:async';
// ignore: depend_on_referenced_packages
import 'package:path/path.dart' show join;
import 'package:todoapp/model/user.dart';
import 'package:todoapp/model/todo.dart';

class DB {
  static Future<Database> initDB() async {
    return openDatabase(join(await getDatabasesPath(), 'todoapp.db'),
        onConfigure: (_) async => await _.execute('PRAGMA foreign_keys = ON'),
        onCreate: (_, version) async {
          await _.execute(
            'CREATE TABLE user(id INTEGER PRIMARY KEY, name TEXT NOT NULL, user TEXT NOT NULL, password TEXT NOT NULL)',
          );
          //sqflite no admite el tipo DATETIME así que guardo en tipo texto
          await _.execute(
            'CREATE TABLE todo(id INTEGER PRIMARY KEY, user_id INTEGER NOT NULL, taskname TEXT NOT NULL, description TEXT NOT NULL,'
            'taskdate TEXT NOT NULL, horasdedicar INTEGER NOT NULL, image BLOB,'
            'FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE)',
          );
        },
        version: 1);
  }

  static Future<int> insertUser(User user) async {
    Database db = await initDB();
    return await db.insert('user', user.toMap());
  }

  static Future<List<User>> getUsuariosBd() async {
    Database db = await initDB();
    final List<Map<String, dynamic>> usuarios = await db.query('user');
    return List.generate(
        usuarios.length,
        (index) => User(usuarios[index]['name'], usuarios[index]['user'],
            usuarios[index]['password']));
  }

  static Future<User> getUsuarioBD({required final int id}) async {
    Database db = await initDB();
    final List<Map<String, dynamic>> usuarios =
        await db.query('user', where: 'id = $id');
    return User(
        usuarios[0]['name'], usuarios[0]['user'], usuarios[0]['password']);
  }

  static Future<dynamic> exist(String user, String passwd) async {
    Database db = await initDB();
    final List<Map<String, dynamic>> usuario = await db.query('user',
        columns: ['id'],
        where: 'user = ? AND password = ?',
        whereArgs: [user, passwd]);
    return usuario[0]['id'];
  }

  //todo task

  static Future<int> insertTask(Todo todo) async {
    Database db = await initDB();
    return await db.insert('todo', todo.toMap());
  }

  static Future<List<Todo>> getTasksBd({required userId}) async {
    Database db = await initDB();
    final List<Map<String, dynamic>> tareas = await db.query('todo', where: 'user_id = ?', whereArgs: [userId]);
    return List.generate(
        tareas.length,
        (index) => Todo(
            id:  tareas[index]['id'],
            userId: tareas[index]['user_id'],
            taskName: tareas[index]['taskname'],
            description: tareas[index]['description'],
            taskDate: DateTime.parse(tareas[index]['taskdate'],),
            horasDedicar: tareas[index]['horasdedicar'],
            imagen: tareas[index]['image']));
  }

  static Future<int> deleteTask(int id) async {
    Database db = await initDB();
    int dato = await db.delete('todo', where: 'id = ?', whereArgs: [id]);
    //log('afectadas: $dato');
    return dato;
  }

  static Future<int> updateTask(Todo todo) async {
    Database db = await initDB();
    return await db.update('todo', todo.toMap(),
        where: 'id = ?', whereArgs: [todo.id]);
  }
}
