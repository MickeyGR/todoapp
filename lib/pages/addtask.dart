import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:todoapp/database/db.dart';
import 'package:todoapp/model/todo.dart';
import 'package:todoapp/widgets/validateimage.dart';

class AddTask extends StatefulWidget {
  const AddTask({Key? key}) : super(key: key);
  @override
  State<AddTask> createState() => _AddTaskState();
}

class _AddTaskState extends State<AddTask> {
  final formKey = GlobalKey<FormState>();
  late String tasknm;
  late String desc;
  late DateTime fechaTarea;
  late int horasDedicar;
  Uint8List? imgnut8;
  final dateTimeController = TextEditingController();
  late int userId;
  //imagen cargada
  bool cargada = false;

  @override
  Widget build(BuildContext context) {
    final dynamic parametros = ModalRoute.of(context)!.settings.arguments;
    userId = parametros['id'];
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Agregar Tarea'),
      ),
      body: SafeArea(
        child: Center(
          child: Form(
              key: formKey,
              child: SingleChildScrollView(
                reverse: true,
                child: addTask(),
              )),
        ),
      ),
    );
  }

  Center cargarImagen() {
    return Center(
        child: Stack(
      alignment: Alignment.bottomLeft,
      children: [ValidateImage.img(imgnut8: imgnut8,cargada: cargada), btnBuscarImagen()],
    ));
  }

  Column addTask() {
    dateTimeController.text = '';
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        cargarImagen(),
        const SizedBox(
          height: 20,
        ),
        taskName(),
        const SizedBox(
          height: 20,
        ),
        description(),
        const SizedBox(
          height: 20,
        ),
        horasADedicar(),
        const SizedBox(
          height: 20,
        ),
        taskDateField(),
        const SizedBox(
          height: 20,
        ),
        submitBtn(),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }

  IconButton btnBuscarImagen() {
    return IconButton(
        onPressed: () async => await filePickerImagen(),
        icon: const Icon(
          Icons.image_search_sharp,
          color: Colors.black,
        ));
  }

  Future filePickerImagen() async {
    try {
      var img = await ImagePicker().pickImage(source: ImageSource.gallery);

      File imageFile = File(img!.path);
      Uint8List imageRaw = await imageFile.readAsBytes();
      setState(() {
        imgnut8 = imageRaw;
        cargada = true;
      });
    } catch (e) {
      //
    }
  }

  Padding taskName() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: TextFormField(
        decoration: const InputDecoration(
            border: OutlineInputBorder(), labelText: 'Nombre de la tarea'),
        onSaved: (value) {
          tasknm = value!;
        },
        validator: (value) {
          return (value!.isEmpty) ? "Ingrese una nombre para la tarea" : null;
        },
      ),
    );
  }

  Padding description() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: TextFormField(
        decoration: const InputDecoration(
            border: OutlineInputBorder(), labelText: 'Descripción'),
        onSaved: (value) {
          desc = value!;
        },
        validator: (value) {
          return (value!.isEmpty) ? "Ingrese una descripción" : null;
        },
      ),
    );
  }

  Padding horasADedicar() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: TextFormField(
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        ],
        decoration: const InputDecoration(
            border: OutlineInputBorder(), labelText: 'Horas a dedicar'),
        onSaved: (value) {
          horasDedicar = int.parse(value!);
        },
        validator: (value) {
          return (value!.isEmpty)
              ? "Ingrese cantidad de horas"
              : (num.tryParse(value)) != null
                  ? null
                  : "Ingrese valor numérico";
        },
      ),
    );
  }

  Padding taskDateField() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: TextFormField(
        controller: dateTimeController,
        readOnly: true,
        decoration: const InputDecoration(
            border: OutlineInputBorder(), labelText: 'Fecha de la tarea'),
        onTap: () => DatePicker.showDateTimePicker(context,
            showTitleActions: true, onChanged: (date) {
          //print('change $date');
        }, onConfirm: (date) {
          // print('confirm $date');
          fechaTarea = date;
          dateTimeController.text =
              '${fechaTarea.day}-${fechaTarea.month}-${fechaTarea.year} ${fechaTarea.hour}:${fechaTarea.minute}';
        }, currentTime: DateTime.now(), locale: LocaleType.es),
        validator: (value) {
          return (value!.isEmpty) ? "Ingrese una fecha" : null;
        },
      ),
    );
  }

  Padding submitBtn() {
    return Padding(
        padding: const EdgeInsets.only(left: 30.0, right: 30.0),
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Colors.blue,
              minimumSize: const Size.fromHeight(50), // NEW
            ),
            onPressed: () async {
              if (formKey.currentState!.validate()) {
                formKey.currentState!.save();
                int idTask = await DB.insertTask(Todo(
                    userId: userId,
                    taskName: tasknm,
                    description: desc,
                    taskDate: fechaTarea,
                    horasDedicar: horasDedicar,
                    imagen: imgnut8));
                // ignore: use_build_context_synchronously
                if (idTask > 0) {
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            title: const Text('¡Listo!'),
                            content: const Text('Tarea se creó correctamente'),
                            actions: [
                              TextButton(
                                  onPressed: () => Navigator.of(context).pop(),
                                  child: const Text('ok'))
                            ],
                          ));
                }
              }
            },
            child: const Text(
              'Guardar',
              style: TextStyle(fontSize: 20),
            )));
  }
}
