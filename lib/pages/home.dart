import 'package:flutter/material.dart';
import 'package:todoapp/database/db.dart';
import 'package:todoapp/model/todo.dart';
import 'package:todoapp/model/user.dart';
import 'package:todoapp/widgets/validateimage.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  //variables para recibir parametros
  late User usuario;
  late int userId;
  //asignaciones de la DB
  List<Todo> tareasBD = [];
  //imagen cargada
  bool cargada = false;

  getTasks() async {
    tareasBD = await DB.getTasksBd(userId: userId);
    setState(() {
      tareasBD;
    });
  }

  @override
  Widget build(BuildContext context) {
    //como paso por parametro el id para no estar consultando
    //hago el siguiente procesamiento de argumentos
    final dynamic parametros = ModalRoute.of(context)!.settings.arguments;
    usuario = parametros['usuario'];
    userId = parametros['id'];
    getTasks();
    return SafeArea(
      child: Scaffold(
          drawer: menu(),
          appBar: AppBar(
            title: const Text('Inicio'),
            actions: [
              IconButton(
                  onPressed: () =>
                      Navigator.restorablePushNamed(context, '/login'),
                  icon: const Icon(Icons.logout))
            ],
          ),
          body: taskList(),
          floatingActionButton: fAB()),
    );
  }

  Drawer menu() {
    return Drawer(
        child: ListView(children: [
      UserAccountsDrawerHeader(
        currentAccountPicture: circleAvatarChar(txt: usuario.name),
        accountName: Text(usuario.name),
        accountEmail: Text(usuario.user),
      ),
      ListTile(
          leading: const Icon(Icons.home),
          title: const Text("Inicio"),
          onTap: () {
            Navigator.of(context).pop();
          }),
      ListTile(
          leading: const Icon(Icons.bar_chart_sharp),
          title: const Text("Gráficos"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).pushNamed('/graphics', arguments:  {'user_id': userId});
          }),
    ]));
  }

  CircleAvatar circleAvatarChar(
      {required String txt,
      double fontsize = 40,
      Color color = Colors.white,
      Color charColor = Colors.blue}) {
    return CircleAvatar(
        backgroundColor: color,
        child: Text(
          txt[0].toUpperCase(),
          style: TextStyle(fontSize: fontsize, color: charColor),
        ));
  }

  FloatingActionButton fAB() {
    return FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () => Navigator.of(context)
            .pushNamed('/addtask', arguments: {'id': userId}));
  }

  ListView taskList() {
    return ListView.builder(
      itemCount: tareasBD.isNotEmpty ? tareasBD.length : 0,
      itemBuilder: (context, index) {
        //item de tarea
        return ListTile(
          onTap: () {
            showDialog(
                context: context,
                builder: (context) => taskDialog(tarea: tareasBD[index]));
          },
          leading: circleAvatarChar(
              txt: tareasBD[index].taskName,
              fontsize: 20,
              color: Colors.blue,
              charColor: Colors.white),
          title: Text(tareasBD[index].taskName),
          subtitle: Text(tareasBD[index].description),
          trailing: PopupMenuButton(
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: const Text('Eliminar'),
                  onTap: () async {
                    await DB.deleteTask(tareasBD[index].id);
                    getTasks();
                  },
                ),
                PopupMenuItem(
                  child: const Text('Editar'),
                  onTap: () async {
                    await Future.delayed(Duration.zero);
                    // ignore: use_build_context_synchronously
                    Navigator.of(context).pushNamed('/edittask',
                        arguments: {'task': tareasBD[index]});
                  },
                )
              ];
            },
          ),
        );
      },
    );
  }

  AlertDialog taskDialog({required Todo tarea}) {
    return AlertDialog(
      title: const Text('Información de la tarea'),
      content: infoTask(tarea: tarea),
      actions: [
        TextButton(
            onPressed: () => Navigator.of(context).pop(),
            child: const Text('Ok'))
      ],
    );
  }

  SingleChildScrollView infoTask({required Todo tarea}) {
    cargada = (tarea.imagen != null) ? true : false;
    return SingleChildScrollView(
      reverse: true,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ValidateImage.img(imgnut8: tarea.imagen, cargada: cargada),
          Text('Nombre: ${tarea.taskName}'),
          Text('Descripción: ${tarea.description}'),
          Text('Horas a dedicar: ${tarea.horasDedicar}'),
          Text('Fecha de inicio: ${tarea.taskDate}'),
        ],
      ),
    );
  }
}
