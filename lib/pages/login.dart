import 'package:flutter/material.dart';
import 'package:todoapp/database/db.dart';
import 'package:todoapp/model/user.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);
  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final formKey = GlobalKey<FormState>();
  late String usr;
  late String passwd;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Form(
              key: formKey,
              child: SingleChildScrollView(
                reverse: true,
                child: wdgLogin(),
              )),
        ),
      ),
    );
  }

  Container wdgLogin() {
    return Container(
      margin: const EdgeInsets.only(top: 40, bottom: 40),
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        const FlutterLogo(
          size: 100,
        ),
        const SizedBox(
          height: 20,
        ),
        const Text(
          'INICIAR SESIÓN',
          style: TextStyle(
              color: Colors.blue, fontSize: 25, fontWeight: FontWeight.bold),
        ),
        const SizedBox(
          height: 40,
        ),
        user(),
        const SizedBox(
          height: 20,
        ),
        password(),
        const SizedBox(
          height: 40,
        ),
        submitBtn(),
        const SizedBox(
          height: 20,
        ),
        signupBtn(),
      ]),
    );
  }

  Padding user() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: TextFormField(
        //style: const TextStyle(fontSize: 20),
        decoration: const InputDecoration(
            border: OutlineInputBorder(), labelText: 'Nombre de usuario'),
        onSaved: (value) {
          usr = value!;
        },
        validator: (value) {
          return (value!.isEmpty) ? "Ingrese el nombre de usuario" : null;
        },
      ),
    );
  }

  Padding password() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: TextFormField(
        decoration: const InputDecoration(
            border: OutlineInputBorder(), labelText: 'Introduce tu contraseña'),
        obscureText: true,
        onSaved: (value) {
          passwd = value!;
        },
        validator: (value) {
          return (value!.isEmpty) ? "Ingrese una contraseña" : null;
        },
      ),
    );
  }

  Padding submitBtn() {
    return Padding(
        padding: const EdgeInsets.only(left: 30.0, right: 30.0),
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Colors.blue,
              minimumSize: const Size.fromHeight(50),
            ),
            onPressed: () async {
              if (formKey.currentState!.validate()) {
                formKey.currentState!.save();
                dynamic id = await DB.exist(usr, passwd);
                User usuario = await DB.getUsuarioBD(id: id);
                if(id!=null){
                  // ignore: use_build_context_synchronously
                  Navigator.of(context).pushNamed('/home', arguments: {'id':id, 'usuario':usuario});
                }
                
              }
            },
            child: const Text(
              'Iniciar sesión',
              style: TextStyle(fontSize: 20),
            )));
  }

  Padding signupBtn() {
    return Padding(
        padding: const EdgeInsets.only(left: 30.0, right: 30.0),
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Colors.grey[400],
              minimumSize: const Size.fromHeight(50),
            ),
            onPressed: () => Navigator.of(context).pushNamed('/signup'),
            child: const Text(
              'Regístrate',
              style: TextStyle(fontSize: 20),
            )));
  }
}
