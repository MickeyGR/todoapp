import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:todoapp/database/db.dart';
import 'package:todoapp/model/todo.dart';
import 'package:todoapp/widgets/validateimage.dart';

class EditTask extends StatefulWidget {
  const EditTask({Key? key}) : super(key: key);

  @override
  State<EditTask> createState() => _EditTaskState();
}

class _EditTaskState extends State<EditTask> {
  final formKey = GlobalKey<FormState>();
  final dateTimeController = TextEditingController();
  late Todo tarea;
  late String tasknm;
  late String desc;
  late DateTime fechaTarea;
  late int horasDedicar;
  //imagen cargada
  bool cargada = false;
  @override
  Widget build(BuildContext context) {
    final dynamic parametros = ModalRoute.of(context)!.settings.arguments;
    tarea = parametros['task'];
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Editar Tarea'),
      ),
      body: SafeArea(
        child: Center(
          child: Form(
              key: formKey,
              child: SingleChildScrollView(
                reverse: true,
                child: editTask(),
              )),
        ),
      ),
    );
  }

  Column editTask() {
    dateTimeController.text = tarea.taskDate.toString();
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        cargarImagen(),
        const SizedBox(
          height: 20,
        ),
        taskName(),
        const SizedBox(
          height: 20,
        ),
        description(),
        const SizedBox(
          height: 20,
        ),
        horasADedicar(),
        const SizedBox(
          height: 20,
        ),
        taskDateField(),
        const SizedBox(
          height: 20,
        ),
        submitBtn(),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }

  Center cargarImagen() {
    cargada = (tarea.imagen != null) ? true : false;
    return Center(
        child: Stack(
      alignment: Alignment.bottomLeft,
      children: [
        ValidateImage.img(imgnut8: tarea.imagen, cargada: cargada),
        btnBuscarImagen()
      ],
    ));
  }

  IconButton btnBuscarImagen() {
    return IconButton(
        onPressed: () async => await filePickerImagen(),
        icon: const Icon(
          Icons.image_search_sharp,
          color: Colors.black,
        ));
  }

  Future filePickerImagen() async {
    try {
      var img = await ImagePicker().pickImage(source: ImageSource.gallery);

      File imageFile = File(img!.path);
      Uint8List imageRaw = await imageFile.readAsBytes();
      setState(() {
        tarea.imagen = imageRaw;
        cargada = true;
      });
    } catch (e) {
      //
    }
  }

  Padding taskName() {
    tasknm = tarea.taskName;
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: TextFormField(
        initialValue: tarea.taskName,
        decoration: const InputDecoration(
            border: OutlineInputBorder(), labelText: 'Nombre de la tarea'),
        onSaved: (value) {
          tasknm = value!;
        },
        validator: (value) {
          return (value!.isEmpty) ? "Ingrese una nombre para la tarea" : null;
        },
      ),
    );
  }

  Padding description() {
    desc=tarea.description;
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: TextFormField(
        initialValue: tarea.description,
        decoration: const InputDecoration(
            border: OutlineInputBorder(), labelText: 'Descripción'),
        onSaved: (value) {
          desc = value!;
        },
        validator: (value) {
          return (value!.isEmpty) ? "Ingrese una descripción" : null;
        },
      ),
    );
  }

  Padding horasADedicar() {
    horasDedicar=tarea.horasDedicar;
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: TextFormField(
        initialValue: tarea.horasDedicar.toString(),
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        ],
        decoration: const InputDecoration(
            border: OutlineInputBorder(), labelText: 'Horas a dedicar'),
        onSaved: (value) {
          horasDedicar = int.parse(value!);
        },
        validator: (value) {
          return (value!.isEmpty)
              ? "Ingrese cantidad de horas"
              : (num.tryParse(value)) != null
                  ? null
                  : "Ingrese valor numérico";
        },
      ),
    );
  }

  Padding taskDateField() {
    fechaTarea = tarea.taskDate;
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: TextFormField(
        controller: dateTimeController,
       // initialValue: '${tarea.taskDate.day}-${tarea.taskDate.month}-${tarea.taskDate.year} ${tarea.taskDate.hour}:${tarea.taskDate.minute}',
        readOnly: true,
        decoration: const InputDecoration(
            border: OutlineInputBorder(), labelText: 'Fecha de la tarea'),
        onTap: () => DatePicker.showDateTimePicker(context,
            showTitleActions: true, onChanged: (date) {}, onConfirm: (date) {
          fechaTarea = date;
          dateTimeController.text =
              '${fechaTarea.day}-${fechaTarea.month}-${fechaTarea.year} ${fechaTarea.hour}:${fechaTarea.minute}';
        }, currentTime: DateTime.now(), locale: LocaleType.es),
        validator: (value) {
          return (value!.isEmpty) ? "Ingrese una fecha" : null;
        },
      ),
    );
  }

  Padding submitBtn() {
    return Padding(
        padding: const EdgeInsets.only(left: 30.0, right: 30.0),
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Colors.blue,
              minimumSize: const Size.fromHeight(50), // NEW
            ),
            onPressed: () async {
              if (formKey.currentState!.validate()) {
                formKey.currentState!.save();
                int idTask = await DB.updateTask(Todo(
                    id: tarea.id,
                    userId: tarea.userId,
                    taskName: tasknm,
                    description: desc,
                    taskDate: fechaTarea,
                    horasDedicar: horasDedicar,
                    imagen: tarea.imagen));
                if (idTask > 0) {
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            title: const Text('¡Listo!'),
                            content:
                                const Text('Tarea se actualizó correctamente'),
                            actions: [
                              TextButton(
                                  onPressed: () => Navigator.of(context).pop(),
                                  child: const Text('ok'))
                            ],
                          ));
                }
              }
            },
            child: const Text(
              'Actualizar',
              style: TextStyle(fontSize: 20),
            )));
  }
}
