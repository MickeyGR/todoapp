import 'package:flutter/material.dart';
import 'package:todoapp/database/db.dart';
import 'package:todoapp/model/user.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final formKey = GlobalKey<FormState>();
  late String nomb;
  late String usr;
  late String passwd;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Registrarse'),
      ),
      body: SafeArea(
        child: Center(
          child: Form(
              key: formKey,
              child: SingleChildScrollView(
                reverse: true,
                child: wdgSignUp(),
              )),
        ),
      ),
    );
  }

  Container wdgSignUp() {
    return Container(
      margin: const EdgeInsets.only(top: 40, bottom: 40),
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        name(),
        const SizedBox(
          height: 20,
        ),
        user(),
        const SizedBox(
          height: 20,
        ),
        password(),
        const SizedBox(
          height: 40,
        ),
        submitBtn(),
        const SizedBox(
          height: 20,
        ),
      ]),
    );
  }

  Padding name() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: TextFormField(
        //style: const TextStyle(fontSize: 20),
        decoration: const InputDecoration(
            border: OutlineInputBorder(), labelText: 'Nombre completo'),
        onSaved: (value) {
          nomb = value!;
        },
        validator: (value) {
          return (value!.isEmpty) ? "Ingrese nombres y apellidos" : null;
        },
      ),
    );
  }

  Padding user() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: TextFormField(
        //style: const TextStyle(fontSize: 20),
        decoration: const InputDecoration(
            border: OutlineInputBorder(), labelText: 'Nombre de usuario'),
        onSaved: (value) {
          usr = value!;
        },
        validator: (value) {
          return (value!.isEmpty) ? "Ingrese el nombre de usuario" : null;
        },
      ),
    );
  }

  Padding password() {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: TextFormField(
        decoration: const InputDecoration(
            border: OutlineInputBorder(), labelText: 'Introduce tu contraseña'),
        onSaved: (value) {
          passwd = value!;
        },
        validator: (value) {
          return (value!.isEmpty) ? "Ingrese una contraseña" : null;
        },
      ),
    );
  }

  Padding submitBtn() {
    return Padding(
        padding: const EdgeInsets.only(left: 30.0, right: 30.0),
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Colors.blue,
              minimumSize: const Size.fromHeight(50), // NEW
            ),
            onPressed: () async {
              if (formKey.currentState!.validate()) {
                formKey.currentState!.save();
                int id = await DB.insertUser(User(nomb, usr, passwd));
                if (id > 0) {
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            title: const Text('¡Listo!'),
                            content: const Text('Usuario se creó correctamente'),
                            actions: [
                              TextButton(
                                  onPressed: () => Navigator.of(context).pop(), child: const Text('Ok'))
                            ],
                          ));
                }
              }
            },
            child: const Text(
              'Guardar',
              style: TextStyle(fontSize: 20),
            )));
  }
}
