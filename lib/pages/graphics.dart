import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:todoapp/api/api.dart';
import 'package:todoapp/database/db.dart';
import 'package:todoapp/model/daily_sales_data.dart';
import 'package:todoapp/model/todo.dart';
import 'package:intl/intl.dart' show DateFormat;

class Graphics extends StatefulWidget {
  const Graphics({Key? key}) : super(key: key);

  @override
  State<Graphics> createState() => _GraphicsState();
}

class _GraphicsState extends State<Graphics> {
  late TooltipBehavior tooltipBehavior;
  List<DailySalesData> dailyList = [];
  List<Todo> tareasBD = [];
  List<SerieDate> serieData=[];
  late int userId;
  @override
  void initState() {
    super.initState();
    tooltipBehavior = TooltipBehavior(enable: true);
    getDatosApi();
  }

  Future<void> getDatosApi() async {
    dailyList = await Api.getDailySD();
    setState(() {
      dailyList;
    });
  }

  getTasks() async {
    tareasBD = await DB.getTasksBd(userId: userId);
    setState(() {
      tareasBD;
    });
  }

  @override
  Widget build(BuildContext context) {
    final dynamic parametros = ModalRoute.of(context)!.settings.arguments;
    userId = parametros['user_id'];
    getTasks();
    contarTareaXFecha();
    return DefaultTabController(
        length: 2,
        child: SafeArea(
          child: Scaffold(
              appBar: AppBar(
                title: const Text('Gráficos'),
                centerTitle: true,
                bottom: const TabBar(tabs: [
                  Tab(
                    icon: Icon(Icons.bar_chart_sharp),
                  ),
                  Tab(
                    icon: Icon(Icons.show_chart),
                  )
                ]),
              ),
              body: TabBarView(
                children: [
                  Column(
                    children: [
                      const SizedBox(
                        height: 40,
                      ),
                      Container(
                          padding: const EdgeInsets.all(30),
                          height: 300,
                          child: barChart()),
                    ],
                  ),
                  Column(
                    children: [
                      const SizedBox(
                        height: 40,
                      ),
                      Container(
                          padding: const EdgeInsets.all(30),
                          height: 300,
                          child: lineChart()),
                    ],
                  ),
                ],
              )),
        ));
  }

  SfCartesianChart barChart() {
    return SfCartesianChart(
      tooltipBehavior: tooltipBehavior,
      primaryXAxis: CategoryAxis(),
      series: [
        StackedColumnSeries(
            enableTooltip: true,
            pointColorMapper: (DailySalesData series, _) => series.color,
            dataSource: dailyList,
            xValueMapper: (DailySalesData series, _) => series.date,
            yValueMapper: (DailySalesData series, _) => series.units)
      ],
    );
  }

  SfCartesianChart lineChart() {
    return SfCartesianChart(
      primaryXAxis: DateTimeAxis(), series: <ChartSeries>[
      // Renders line chart
      LineSeries<SerieDate, DateTime>(
          dataSource: serieData,
          xValueMapper: (SerieDate sales, _) => sales.fecha,
          yValueMapper: (SerieDate sales, _) => sales.cantidad)
    ]);
  }

  void contarTareaXFecha() {
    int contador = 0;
    List<Map<String, int>> serie = [];
    for (Todo fecha in tareasBD) {
      fecha.taskDate;
      for (int i = 0; i < tareasBD.length; i++) {
        if (tareasBD[i].taskDate.year == fecha.taskDate.year &&
            tareasBD[i].taskDate.month == fecha.taskDate.month &&
            tareasBD[i].taskDate.day == fecha.taskDate.day) {
          contador++;
        }
      }
      serie.add({
        '${fecha.taskDate.year}-${fecha.taskDate.month}-${fecha.taskDate.day}':contador
      });
      contador = 0;
    }
    //filtrando 
    final Map<String, dynamic> mapFilter = {};
    for (Map<String, int> item in serie) {
      mapFilter[item.keys.toString().replaceAll("(", "").replaceAll(")", "")] = item;
    }
    List<Map<String, int>> filtrados = mapFilter.keys.map((key) => mapFilter[key] as Map<String,int>).toList();
    //creando serie de datos
    DateTime fecha;
    int cant;
    for (Map<String, int> item in filtrados) {
      var fechaFormato = DateFormat('yyyy/MM/dd').parse(item.keys.toString().replaceAll("(", "").replaceAll(")", "").replaceAll("-", "/"));
      fecha = DateTime.parse(fechaFormato.toString());
      cant = item[item.keys.toString().replaceAll("(", "").replaceAll(")", "")] as int;
      serieData.add(SerieDate(fecha: fecha, cantidad: cant));
    }
    setState(() {
      serieData;
    });
  }
  
}

class SerieDate {
  late DateTime fecha;
  late int cantidad;
  SerieDate({required this.fecha, required this.cantidad});
}