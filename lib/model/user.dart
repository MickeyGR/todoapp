class User {
  late String name;
  late String user;
  late String password;

  User(this.name, this.user, this.password);
  
  Map<String, dynamic> toMap(){
    return {
      'name':name,
      'user':user,
      'password':password
    };
  }
}