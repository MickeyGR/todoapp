import 'dart:typed_data';

class Todo {
  late int id;
  late int userId;
  late String taskName;
  late String description;
  late DateTime taskDate;
  late int horasDedicar;
  late Uint8List? imagen;


  Todo({this.id=-1,required this.userId, required this.taskName, required this.description, required this.taskDate, required this.horasDedicar, this.imagen});
  
  Map<String, dynamic> toMap(){
    return {
      'user_id':userId,
      'taskname':taskName,
      'description':description,
      'taskdate':taskDate.toString(),
      'horasdedicar':horasDedicar,
      'image':imagen
    };
  }
}