import 'package:flutter/material.dart';

class DailySalesData {
  late int date;
  late int units;
  late Color color;
  DailySalesData({required this.date, required this.units, this.color=Colors.blue});
}