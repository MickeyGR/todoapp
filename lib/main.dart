import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:todoapp/pages/addtask.dart';
import 'package:todoapp/pages/edittask.dart';
import 'package:todoapp/pages/graphics.dart';
import 'package:todoapp/pages/home.dart';
import 'package:todoapp/pages/signup.dart';

import 'pages/login.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.removeAfter(initialization);
  runApp(const MyApp());
}

Future initialization(BuildContext context) async {
  await Future.delayed(const Duration(seconds: 3));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'To Do App',
      routes: {
        '/login': (context) => const Login(),
        '/signup': (context) => const SignUp(),
        '/home':(context) =>  const Home(),
        '/graphics':(context) =>  const Graphics(),
        '/addtask':(context) =>  const AddTask(),
        '/edittask':(context) =>  const EditTask(),
      },
      initialRoute: '/login',
    );
  }
}
