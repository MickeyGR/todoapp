import 'dart:typed_data';

import 'package:flutter/material.dart';

class ValidateImage {
  static Widget img({Uint8List? imgnut8, required bool cargada}) {
    if (imgnut8 != null && cargada) {
      return Image.memory(imgnut8, width: 200, height: 200, fit: BoxFit.cover);
    } else {
      return const FlutterLogo(size: 200);
    }
  }
}