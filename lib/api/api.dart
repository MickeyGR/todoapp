import 'dart:math';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:todoapp/model/daily_sales_data.dart';

class Api {
  static String url =
      'https://canvasjs.com/data/gallery/jquery/daily-sales-data.json';
  static Future<List<DailySalesData>> getDailySD() async {
    http.Response response = await http.get(Uri.parse(url));
    List<dynamic> dailySD = json.decode(response.body);
    return List.generate(
        dailySD.length,
        (index) => DailySalesData(
            date: dailySD[index]['date'], units: dailySD[index]['units'], color: Colors.primaries[Random().nextInt(Colors.primaries.length)]));
  }
}
